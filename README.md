# Margin Master Style Sheet #

Margins Master Style Sheet can make for a complete substitute for ever having to build Margin Selectors.

### Overview ###

* A syntax standard for allocating CSS style margins around elements.
* Version: 1.0.1
* [Demo](http://sunlandcomputers.com/projects/library/margins/)

### Description ###

Design purpose is to create a syntax standard for allocating margins around elements; thus making it faster and easier to set margins in an HTML page while not having to recreate all the margin distances within an existing or newly built CSS style sheet.

In hopes that this particular syntax is simple and self explanitory enough for every programmer to pick up on, the Margins Master Style Sheet can make for a complete substitute for ever having to build Margin Selectors at all.

All one would have to get use to doing is to write in class=“marg-t1” to create a 1em margin to the top of a div or element - or as another example - class=“marg-tm1” to create a negative margin (t is for top and m is for minus!)

Some examples layout the basis of the whole concept: 
class=“marg-r1” = margin right 1 em

class=“marg-l1” = margin left 1 em

class=“marg-cntr” = centered

class=“marg-bm1” = b for bottom and m for minus. How much easier could it be to remember?

Going forward: marg-l3 = margin-left 3em; marg-r4 = margin-right 4em; marg-tm1 = margin-top minus 1 em; marg-b2 = margin-bottom 2 em.

The lower case L and the lower case R were going to be set as upper case to avoid visual confusion but it was also decided that the adavatange over the speed of typing a lower case letter would override the disadvantage of confusing the letter L in its lower case state when the lower case L will be next to the number 1 (l1 vs. L1).

If there were any way easier I would hope you could use this standard to build all your margins.

* Deployment instructions
Place the import statement into the top of your style sheet to bring all the margin classes into your main style sheet.

Most people are now building a primary style sheet (let us call it master.css) and then import four style sheets into that one sheet.

master.css would look like this:
@import resets.css;
@import type.css;
@import grid.css;
@import margins.css;


By changing some of the em sizes you can make the marg-tm2 equal to margin-top:-1.84em; instead of having it left exactly the margin-top:-2em; that is specified in the original stylesheet by default.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who? ###

* tradesouthwest
* Other community or team contact: Bitbucket repo